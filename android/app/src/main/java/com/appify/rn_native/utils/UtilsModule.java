package com.appify.rn_native.utils;

import android.content.Context;
import android.os.Build;
import android.os.LocaleList;
import android.telecom.Call;
import com.facebook.react.bridge.*;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class UtilsModule extends ReactContextBaseJavaModule {

    private Context mContext;

    public UtilsModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mContext = reactContext;
    }

    /**
     * 根据String key 获得res 中对应的content,以string 的形式返回
     **/
    @ReactMethod
    public void getResString(String str, Callback callback) {
        String s = str;
        int id = mContext.getResources().getIdentifier(str, "string", mContext.getPackageName());
        if (id != 0) {
            s = mContext.getString(id);
        }
        callback.invoke(s);
    }

    @ReactMethod
    public void getLanguage(Callback callback) {
        //获取 Locale 对象的正确姿势：
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = mContext.getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = mContext.getResources().getConfiguration().locale;
        }

        //获取语言的正确姿势：

        String lang = locale.getLanguage() + "-" + locale.getCountry();
        callback.invoke(lang);
    }

    @ReactMethod
    public void getLanguage(Promise promise) {
        //获取 Locale 对象的正确姿势：
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = mContext.getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = mContext.getResources().getConfiguration().locale;
        }

        //获取语言的正确姿势：

        String lang = locale.getLanguage() + "-" + locale.getCountry();
        promise.resolve(lang);
    }

    public String getLanguage() {
        //获取 Locale 对象的正确姿势：
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = mContext.getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = mContext.getResources().getConfiguration().locale;
        }

        //获取语言的正确姿势：

        String lang = locale.getLanguage() + "-" + locale.getCountry();
       return lang;
    }


    private String toLanguageTag(Locale locale) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return locale.toLanguageTag();
        }

        StringBuilder builder = new StringBuilder();
        builder.append(locale.getLanguage());

        if (locale.getCountry() != null) {
            builder.append("-");
            builder.append(locale.getCountry());
        }

        return builder.toString();
    }

    private WritableArray getLocaleList() {
        WritableArray array = Arguments.createArray();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            LocaleList locales = getReactApplicationContext()
                    .getResources().getConfiguration().getLocales();

            for (int i = 0; i < locales.size(); i++) {
                array.pushString(this.toLanguageTag(locales.get(i)));
            }
        } else {
            array.pushString(this.toLanguageTag(getReactApplicationContext()
                    .getResources().getConfiguration().locale));
        }

        return array;
    }

    @Override
    public String getName() {
        return "UtilsAndroid";
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        HashMap<String, Object> constants = new HashMap<String,Object>();
        constants.put("language", this.getLanguage());
        return constants;
    }
}
