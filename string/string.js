import UtilsAndroid from '../native/native'
import Config from './config'

let language = UtilsAndroid.language;

export default class String  {
    static getString(key) {
        console.log("get string key :" + key);
        console.log("language :" + language);
        let languagePackage = require("./en");
        try {
            switch (language) {
                case Config.EN_US:
                    languagePackage = require("./en");
                    break;
                case Config.ZH_CN:
                    languagePackage = require("./zh");
                    break;
            }
        }
        catch (e) {
            console.log("===languagePackageException===%s",e);
        }
        console.log("===111===%s",languagePackage[key]);
        if(languagePackage[key] == null || languagePackage[key] === ""){
            if(language === Config.EN_US){
                languagePackage = require("./zh");
            }else{
                languagePackage = require("./en");
            }
        }
        console.log("======%s",languagePackage[key]);
        return languagePackage[key];
    }
};