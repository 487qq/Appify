module.exports = {
    welcome:'Welcome to',
    appName:'Appify',
    welcome_content:'MOBILE APP UI KIT',
    facebook:'FACEBOOK',
    google:'GOOGLE',
    in:'SIGN IN',
    up:'SIGN UP',
    inTip:'Sign in to continue',
    email:'Email',
    password: 'Password',
};