import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    Button,
    TextInput,
    TouchableOpacity,
    Alert,
    View,
    Image,
    ScrollView,

} from 'react-native'
import  {RadioGroup,RadioButton} from 'react-native-flexi-radio-button'
import {NavigationActions} from 'react-navigation'
import {TextWithLetterSpacing} from "../../self/TextWithLetterSpacing"
import String from '../../string/string'
import ImageEquallyEnlarge from "../../self/ImageEquallyEnlarge";
// import HtmlText from 'react-native-htmltext'


var bg = require('../../img/signin_bg.png');
var google = require('../../img/google.png');
var facebook = require('../../img/facebook.png');
// var html = '<p>Hello world <b>world</b> <i>foo</i> bar hahh</p>';
export default class SignIn extends Component {
    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            email: '',
            password: ''
        };
    }

    render() {
        return (<View style={styles.main}>
            <Image style={styles.bg} source={bg}>
                <ScrollView style={styles.sv}>
                    <View style={{flex: 1, alignItems: 'center', width: '100%', height: '100%'}}>
                        <Text style={styles.app_name}>{String.getString('appName')}</Text>
                        <Text style={styles.welcome_content}>{String.getString('welcome_content')}</Text>
                        <Text style={styles.in_tip}>{String.getString('inTip')}</Text>
                        <TextInput
                            style={styles.input}
                            onChangeText={(text) => this.setState({email: text})}
                            placeholder={String.getString('email')}
                            placeholderTextColor="#D19841"
                            underlineColorAndroid="#D19841"
                            value={this.state.email}
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={(text) => this.setState({password: text})}
                            placeholder={String.getString('password')}
                            placeholderTextColor="#D19841"
                            underlineColorAndroid="#D19841"
                            value={this.state.password} secureTextEntry={true}
                        />
                        <TouchableOpacity onPress={googleBtn} style={styles.btn_google}>
                            {/*<View*/}
                            {/*style={{*/}
                            {/*display: 'flex',*/}
                            {/*flexDirection: 'row',*/}
                            {/*justifyContent: 'flex-start',*/}
                            {/*alignItems: 'flex-start'*/}
                            {/*}}>*/}
                            {/*<ImageEquallyEnlarge*/}
                            {/*originalHeight={40}*/}
                            {/*originalWidth={40}*/}
                            {/*source={google} style={{width: 20, height: 20}}/>*/}
                            {/*<TextWithLetterSpacing spacing={1} textStyle={{*/}
                            {/*marginLeft: 4,*/}
                            {/*fontWeight: 'bold',*/}
                            {/*color: '#de4b39'*/}
                            {/*}}>{String.getString('in')}</TextWithLetterSpacing>*/}
                            {/*</View>*/}
                            <TextWithLetterSpacing spacing={1} textStyle={{
                                marginLeft: 4,
                                fontWeight: 'bold',
                                color: '#de4b39'
                            }}>{String.getString('in')}</TextWithLetterSpacing>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'center',
                        flex: 0,
                        marginBottom: 100
                    }}>
                        <RadioButton value={'item1'} >
                            <Text style={{
                                flex: 1,
                                textAlign: 'center',
                                fontWeight: 'bold',
                                color: 'white'
                            }}>{String.getString('in')}</Text>
                        </RadioButton>


                        <Text style={{
                            flex: 1,
                            textAlign: 'center',
                            fontWeight: 'bold',
                            color: 'white'
                        }}>{String.getString('up')}</Text>
                    </View>

                </ScrollView>
            </Image>
        </View>);
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }


}

const facebookBtn = () => {
    Alert.alert("facebook btn is pressed!")
};

const googleBtn = () => {
    Alert.alert("google btn is pressed!")
};


var styles = StyleSheet.create({
    main: {
        // display: 'flex',
        // flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: '#2480ff'
    },
    sv: {
        width: '100%',
        height: '100%',
    },
    bg: {
        width: '100%',
        height: '100%',
        backgroundColor: '#2eff27'
    },
    app_name: {
        fontSize: 80,
        color: '#FFFFFF',
        alignSelf: 'center',
        // backgroundColor:'#fffaaa'
        fontWeight: '100',
        marginTop: 60

    },
    welcome_content: {
        fontSize: 20,
        color: '#ffffff',
        alignSelf: 'center',
    },
    input:{
        height: 60,
        width: '80%',
        borderColor: 'transparent',
        marginTop: 10,
        borderWidth: 1,
        color:'#D19841',
        fontSize:18,
        display:'flex',
        alignItems:'center',
        textAlign:'center',

    },
    in_tip: {
        marginTop: 30,
        fontSize: 20,
        color: '#ffffff',
        alignSelf: 'center',
    },
    btn_facebook: {
        display: 'flex',
        flexDirection: 'row',
        borderRadius: 25,
        backgroundColor: 'white',
        width: 150,
        padding: 15,
        justifyContent: 'center',
        marginTop: 80
    },
    btn_google: {
        display: 'flex',
        flexDirection: 'row',
        borderRadius: 25,
        backgroundColor: 'white',
        width: 150,
        padding: 15,
        justifyContent: 'center',
        marginTop: 20
    }

});