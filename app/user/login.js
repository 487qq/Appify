import String from '../../string/string'
import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    Button,
    TouchableHighlight,
    TouchableOpacity,
    Alert,
    View,
    Image,
} from 'react-native'
import {NavigationActions} from 'react-navigation'

import ImageEquallyEnlarge from "../../self/ImageEquallyEnlarge";
import {TextWithLetterSpacing} from "../../self/TextWithLetterSpacing"


var bg = require('../../img/login_bg.png');
var google = require('../../img/google.png');
var facebook = require('../../img/facebook.png');
export default class Login extends Component {
    constructor(props) {
        super(props);
        var name = String.getString('facebook');
        console.log('name:%s', name)
    }

    render() {
        // const {navigate} = this.props.navigation;
        return (<View style={styles.main}>
            <Image style={styles.bg} source={bg}>
                <View style={{flex: 1, alignItems: 'center'}}>
                    <Text style={styles.app_name}>{String.getString('appName')}</Text>
                    <Text style={styles.welcome_content}>{String.getString('welcome_content')}</Text>
                    <TouchableOpacity onPress={this.onFacebookBtn} style={styles.btn_facebook}>
                        <View
                            style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'flex-start',
                                alignItems: 'flex-start'
                            }}>
                            <ImageEquallyEnlarge
                                originalHeight={40}
                                originalWidth={40}
                                source={facebook} style={{width: 20, height: 20}}/>

                            <TextWithLetterSpacing spacing={1} textStyle={{
                                marginLeft: 2,
                                fontWeight: 'bold',
                                color: '#385a99'
                            }}>{String.getString('facebook')}</TextWithLetterSpacing>
                        </View>


                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.onGoogleBtn} style={styles.btn_google}>
                        <View
                            style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'flex-start',
                                alignItems: 'flex-start'
                            }}>
                            <ImageEquallyEnlarge
                                originalHeight={40}
                                originalWidth={40}
                                source={google} style={{width: 20, height: 20}}/>
                            <TextWithLetterSpacing spacing={1} textStyle={{
                                marginLeft: 4,
                                fontWeight: 'bold',
                                color: '#de4b39'
                            }}>{String.getString('google')}</TextWithLetterSpacing>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    flex: 0,
                    marginBottom: 100
                }}>
                    <TouchableOpacity onPress={this.onSignIn.bind(this)}
                                      style={{
                                          display: 'flex',
                                          flex: 1,
                                          alignItems: 'center',
                                          paddingTop: 10,
                                          paddingBottom: 10
                                      }}>
                        <Text style={{

                            textAlign: 'center',
                            fontWeight: 'bold',
                            color: 'white'
                        }}>{String.getString('in')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.onSignOut.bind(this)}
                                      style={{
                                          display: 'flex',
                                          flex: 1,
                                          alignItems: 'center',
                                          paddingTop: 10,
                                          paddingBottom: 10
                                      }}>
                        <Text style={{
                            textAlign: 'center',
                            fontWeight: 'bold',
                            color: 'white'
                        }}>{String.getString('up')}</Text>
                    </TouchableOpacity>
                </View>
            </Image>
        </View>);
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }


    onFacebookBtn = () => {
        Alert.alert("facebook btn is pressed!")
    };

    onGoogleBtn = () => {
        Alert.alert("google btn is pressed!")
    };

    onSignIn = () => {
        console.log("========signIn======");
        console.log(this.props);
        console.log("========signIn======");
        this.props.navigation.navigate('SignIn')
    };

    onSignOut = () => {

    };


}


var styles = StyleSheet.create({
    main: {
        display: 'flex',
        flex: 1
    },
    bg: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        alignItems: 'center'
    },
    app_name: {
        fontSize: 80,
        color: '#FFFFFF',
        alignSelf: 'center',
        // backgroundColor:'#fffaaa'
        fontWeight: '100',
        marginTop: 100

    },
    welcome_content: {
        fontSize: 20,
        color: '#ffffff',
        alignSelf: 'center',
    },
    btn_facebook: {
        display: 'flex',
        flexDirection: 'row',
        borderRadius: 25,
        backgroundColor: 'white',
        width: 150,
        padding: 15,
        justifyContent: 'center',
        marginTop: 80
    },
    btn_google: {
        display: 'flex',
        flexDirection: 'row',
        borderRadius: 25,
        backgroundColor: 'white',
        width: 150,
        padding: 15,
        justifyContent: 'center',
        marginTop: 20
    }

});