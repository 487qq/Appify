/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import String from './string/string'
import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';
import {StackNavigator, NavigationActions} from 'react-navigation'
import Login from './app/user/login'

import SignIn from "./app/user/signIn";


var bg = require('./img/splash.png');

export default class Appify extends Component {


    static navigationOptions = {title: 'Welcome', header: null};

    constructor(props) {
        super(props);
        this.state = {lang: ''};
        // UtilsAndroid.getResString('app_name',(msg) =>{this.setState({appName:msg})});
        name = String.getString('welcome');
        console.log("constructor=%s",name);
    }


    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.bg_image} source={bg}>
                    <Text style={styles.welcome}>{String.getString('welcome')}</Text>
                    <Text style={styles.app_name}>{String.getString('appName')}</Text>
                    <Text style={styles.welcome_content}>{String.getString('welcome_content')}</Text>
                    {/*<Button*/}
                    {/*onPress={() => this.props.navigation.navigate('Login')}*/}
                    {/*title="Chat with Lucy"*/}
                    {/*/>*/}
                </Image>
            </View>
        );
    }

    go2Login() {
        this.resetNavigation();
        // this.props.navigation.navigate('Login');
    }

    resetNavigation() {
        console.log("resetNavigation");
        this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'Login'})]
        }));
    }

    componentWillUnmount() {
        console.log("componentWillUnmount=");
        this.timer && clearTimeout(this.timer);
    }

    componentWillMount() {
        console.log("componentWillMount=");
        this.timer = setTimeout(
            () => {
                console.log('I do not leak!');
                this.go2Login();
            },
            2000
        );
    }

    componentDidMount() {
        console.log("componentDidMount");
        // this.resetNavigation();

    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log("shouldComponentUpdate");
        return true
    }

    componentWillUpdate(nextProps, nextState) {
        console.log("componentWillUpdate");
    }

    componentDidUpdate(prevProps, prevState) {
        console.log("componentDidUpdate");
    }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bg_image: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',


    },
    welcome: {
        fontSize: 35,
        color: '#ffffff',
        marginTop: '42%',
        alignSelf: 'center',
        fontStyle: 'italic'
        // backgrou ndColor:'#fffaaa'
    },
    app_name: {
        fontSize: 120,
        color: '#FFFFFF',
        alignSelf: 'center',
        // backgroundColor:'#fffaaa'
        fontWeight: '100',

    },
    welcome_content: {
        fontSize: 35,
        color: '#ffffff',
        alignSelf: 'center',
    }
});
const App = StackNavigator({
    Main: {screen: Appify},
    Login: {
        screen: Login,
        navigationOptions: {
            header:null
        },
    },
    SignIn:{
        screen:SignIn,
        navigationOptions:{
            title:'Sign In',
            header:null
        }
    }

}, {initialRouteName: 'Main'});

AppRegistry.registerComponent('Appify', () => App);
